// Constants
exports.ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";

// Utils

// Returns a 40 length random hexadecimal string
const randHex = len => {
  let maxlen = 8,
    min = Math.pow(16, Math.min(len, maxlen) - 1);
  (max = Math.pow(16, Math.min(len, maxlen)) - 1),
    (n = Math.floor(Math.random() * (max - min + 1)) + min),
    (r = n.toString(16));
  while (r.length < len) {
    r = r + randHex(len - maxlen);
  }
  return r;
};

exports.getRandomInt = function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
};

exports.getRandomAddress = () => {
  return "0x" + randHex(40);
};


exports.getTestnetLegacyDeposit = (accounts) => {
  return accounts[1];
}

exports.getTestnetNewDeposit = (accounts) => {
  return accounts[2];
}