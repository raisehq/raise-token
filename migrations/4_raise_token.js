const { HERO_TOKEN_ADDRESS_MAINNET, HERO_TOKEN_ADDRESS_KOVAN } = require("../config");

const ERC20Migrator = artifacts.require("ERC20Migrator");
const LegacyToken = artifacts.require("LegacyToken");
const Raise = artifacts.require("RaiseToken");

const getParams = (network, erc20MigratorAddress) => {
  if (network.includes("mainnet")) {
    return [HERO_TOKEN_ADDRESS_MAINNET, erc20MigratorAddress];
  } else if (network.includes("kovan-migration")) {
    return [HERO_TOKEN_ADDRESS_KOVAN, erc20MigratorAddress];
  } else {
    return [LegacyToken.address, erc20MigratorAddress];
  }
}

module.exports = async function (deployer, network, accounts) {
  const ERC20MigratorInstance = await ERC20Migrator.deployed();
  const raiseParams = getParams(network, ERC20Migrator.address);
  console.log(`[ERC20MIGRATOR] Deploying Raise token at ${network}`);
  console.log(`[ERC20MIGRATOR] Raise token params:`, raiseParams);
  await deployer.deploy(Raise, ...raiseParams);
};
