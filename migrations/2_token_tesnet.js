const LegacyToken = artifacts.require("LegacyToken");
const Bluebird = require("bluebird");
const {toWei} = require("web3-utils");
const {getRandomInt, getTestnetNewDeposit} = require("../scripts/utils");

// At testnets we simulate legacyDeposit and newDeposit in the following way
// LegacyDeposit is index 1, gets old legacy token airdrop but no new token airdrop
// NewDeposit is index 2, gets new token airdrop but no legacy token airdrop

module.exports = async function(deployer, network, accounts) {
  if (network.includes("mainnet") || network.includes("kovan-migration")) {
    console.log("[ERC20MIGRATOR] Not deploying legacy token in mainnet network");
    return;
  }
  console.log(
    "Deploying FAKE ERC20 to test migration in local. This token mimics the legacy token."
  );
  const newDepositAddress = getTestnetNewDeposit(accounts);
  await deployer.deploy(LegacyToken);
  const LegacyTokenOld = await LegacyToken.deployed();

  console.log("[ERC20MIGRATOR] Making transfers...");
  const wallets = accounts.filter(item => item !== newDepositAddress);
  await Bluebird.mapSeries(wallets, account =>
    LegacyTokenOld.transfer(account, toWei(getRandomInt(100, 1000).toString(), "ether"))
  );
};
