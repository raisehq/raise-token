const ERC20Migrator = artifacts.require("ERC20Migrator");
const LegacyToken = artifacts.require("LegacyToken");

const { HERO_TOKEN_ADDRESS_MAINNET, HERO_TOKEN_ADDRESS_KOVAN, LEGACY_DEPOSIT_ADDRESS_MAINNET, LEGACY_DEPOSIT_ADDRESS_KOVAN } = require("../config");
const { getTestnetLegacyDeposit } = require("../scripts/utils");

const getParams = (network) => {
  if (network.includes("mainnet")) {
    return [HERO_TOKEN_ADDRESS_MAINNET, LEGACY_DEPOSIT_ADDRESS_MAINNET];
  } else if (network.includes("kovan-migration")) {
    return [HERO_TOKEN_ADDRESS_KOVAN, LEGACY_DEPOSIT_ADDRESS_KOVAN];
  } else {
    return [LegacyToken.address, legacyDepositAddress];
  }
}

module.exports = async function (deployer, network, accounts) {
  const params = getParams(network);
  console.log(`[ERC20MIGRATOR] Deploying ERC20 Migrator token at ${network}`);
  console.log("[ERC20MIGRATOR] ERC20Migrator params:", ...params);
  await deployer.deploy(ERC20Migrator, ...params);
};
