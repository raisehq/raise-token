const fs = require("fs");
const util = require("util");
const Bluebird = require("bluebird");
const chunk = require("lodash.chunk");
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
const {
  ZERO_ADDRESS,
  getTestnetNewDeposit,
  LEGACY_DEPOSIT_ADDRESS_MAINNET,
  NEW_DEPOSIT_ADDRESS_MAINNET,
  LEGACY_DEPOSIT_ADDRESS_KOVAN,
  NEW_DEPOSIT_ADDRESS_KOVAN
} = require("../config");

const ERC20Migrator = artifacts.require("ERC20Migrator");
const Raise = artifacts.require("RaiseToken");
const depositArtifact = require("../abis/DepositRegistry");
const readFile = util.promisify(fs.readFile);

const getDepositAdresses = network => {
  if (network.includes("mainnet")) {
    return [LEGACY_DEPOSIT_ADDRESS_MAINNET, NEW_DEPOSIT_ADDRESS_MAINNET];
  } else if (network.includes("kovan-migration")) {
    return [LEGACY_DEPOSIT_ADDRESS_KOVAN, NEW_DEPOSIT_ADDRESS_KOVAN];
  }
  throw new Error("Not for normal migration");
};

const getLegacyDepositors = async (addressList, legacyDepositAddress, newDepositAddress) => {
  const LegacyDeposit = new web3.eth.Contract(depositArtifact.abi, legacyDepositAddress);
  const NewDeposit = new web3.eth.Contract(depositArtifact.abi, newDepositAddress);
  return Bluebird.filter(
    addressList,
    async (address, index) => {
      await sleep(100);
      console.log("Check if depositor", index, "of", addressList.length);
      const legacyDeposited = await LegacyDeposit.methods.hasDeposited(address).call();
      const notNewDeposited = !(await NewDeposit.methods.hasDeposited(address).call());
      return legacyDeposited && notNewDeposited;
    },
    {concurrency: 50}
  );
};

const getPendingWallets = async (addressList, legacyDepositAddress, newDepositAddress) => {
  const ERC20MigratorInstance = await ERC20Migrator.deployed();
  return Bluebird.filter(
    addressList,
    async (address, index) => {
      if (address && address.toUpperCase() === legacyDepositAddress.toUpperCase()) {
        const legacyMigrated = !(await ERC20MigratorInstance.accountMigrated(newDepositAddress));
        return legacyMigrated;
      }
      const notMigrated = !(await ERC20MigratorInstance.accountMigrated(address));
      return notMigrated;
    },
    {concurrency: 200}
  );
};

const migrateDeposits = async (accounts, addressList, legacyDepositAddress, newDepositAddress) => {
  const NewDeposit = new web3.eth.Contract(depositArtifact.abi, newDepositAddress);
  const isFinished = !(await NewDeposit.methods.migrationAllowed().call());
  if (isFinished) {
    console.log("[ERC20MIGRATOR] Will not pursue the Deposit migration due is already finished.");
    return;
  }
  console.log("[ERC20MIGRATOR] Starting Deposit migration.");
  const legacyDepositors = await getLegacyDepositors(
    addressList,
    legacyDepositAddress,
    newDepositAddress
  );
  const batches = chunk(legacyDepositors, 100);
  await Bluebird.mapSeries(batches, (batch, i) => {
    console.log(`[ERC20MIGRATOR] - Migrating deposit batch #${i + 1} of ${batches.length}`);
    return NewDeposit.methods.migrate(batch, legacyDepositAddress).send({from: accounts[0]});
  });
  await NewDeposit.methods.finishMigration().send({from: accounts[0]});
  console.log("[ERC20MIGRATOR] Deposit Registry Migration Success");
};

const migrateTokenHolders = async (wallets, legacyDepositAddress, newDepositAddress) => {
  const ERC20MigratorInstance = await ERC20Migrator.deployed();
  const isFinished = await ERC20MigratorInstance.migrationFinished();
  if (isFinished) {
    console.log("[ERC20MIGRATOR] Will not pursue the Token migratiom due is already finished.");
    return;
  }
  const RaiseInstance = await Raise.deployed();
  const pendingWallets = await getPendingWallets(wallets, legacyDepositAddress, newDepositAddress);
  console.log("migrating", pendingWallets.length, "of", wallets.length);
  const batches = chunk(pendingWallets, 100);
  const started = await ERC20MigratorInstance.newToken();
  if (started.toUpperCase() === ZERO_ADDRESS.toUpperCase()) {
    await ERC20MigratorInstance.beginMigration(RaiseInstance.address, newDepositAddress);
  }
  await Bluebird.mapSeries(batches, (batch, i) => {
    console.log(`[ERC20MIGRATOR] - Migrating token batch #${i + 1} of ${batches.length}`);
    return ERC20MigratorInstance.migrateBatch(batch);
  });
  await ERC20MigratorInstance.finishMigration();
  console.log("[ERC20MIGRATOR] ERC20 Token Migration Success");
};

const productionMigration = async (network, accounts) => {
  const [legacyDepositAddress, newDepositAddress] = getDepositAdresses(network);
  if (!legacyDepositAddress) {
    throw new Error("LEGACY_DEPOSIT_ADDRESS_$NETWORK must be set");
  }
  if (!newDepositAddress) {
    throw new Error("NEW_DEPOSIT_ADDRESS_$NETWORK must be set");
  }
  const wallets = JSON.parse(await readFile("./address-list.json"));
  await migrateTokenHolders(wallets, legacyDepositAddress, newDepositAddress);
  await migrateDeposits(accounts, wallets, legacyDepositAddress, newDepositAddress);
  console.log("[ERC20MIGRATOR] Successful migration");
};

const testnetMigration = async accounts => {
  const ERC20MigratorInstance = await ERC20Migrator.deployed();
  const RaiseInstance = await Raise.deployed();

  const newDepositAddress = getTestnetNewDeposit(accounts);
  const wallets = accounts.filter(item => item !== newDepositAddress); // Remove item in index 2, simulates legacy deposit contract address
  const batches = chunk(wallets, 100);
  const started = await ERC20MigratorInstance.newToken();
  if (started === ZERO_ADDRESS) {
    await ERC20MigratorInstance.beginMigration(RaiseInstance.address, newDepositAddress);
  }
  await Bluebird.mapSeries(batches, async (batch, i) => {
    console.log(`[ERC20MIGRATOR] - Migrating batch #${i + 1} of ${batches.length}`);
    try {
      return ERC20MigratorInstance.migrateBatch(batch);
    } catch (error) {
      console.error(`[ERC20MIGRATOR] - Error while batch #${i + 1} of ${batches.length}`);
      console.error(`[ERC20MIGRATOR] - Batch #${i + 1} first address: ${batch[0]}`);
      throw error;
    }
  });
  console.log("[ERC20MIGRATOR] Token Migration Success");
};

module.exports = async function(deployer, network, accounts) {
  if (network.includes("mainnet") || network.includes("kovan-migration")) {
    console.log(`[ERC20MIGRATOR] Hero => Raise token migration at ${network} network.`);
    await productionMigration(network, accounts);
    return;
  }
  console.log(
    `[ERC20MIGRATOR] Legacy token => Raise token migration at ${network} network in TESTNET.`
  );
  await testnetMigration(accounts);
};
