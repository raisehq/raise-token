const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
const truffleAssert = require("truffle-assertions");
const Bluebird = require("bluebird");
const chunk = require("lodash.chunk");
const { toWei, fromWei, BN } = require("web3-utils");
const bnChai = require("bn-chai");
const { getRandomInt, getRandomAddress, ZERO_ADDRESS } = require("../scripts/utils");

chai.use(chaiAsPromised);
chai.use(bnChai(BN));

const ERC20MigratorContract = artifacts.require("ERC20Migrator");
const LegacyTokenContract = artifacts.require("LegacyToken");
const RaiseTokenContract = artifacts.require("RaiseToken");

const { expect } = chai;
chai.use(chaiAsPromised);

contract("ERC20Migrator", function (accounts) {
  let ERC20Migrator;
  let LegacyToken;
  let TokenNotOwner;
  let RaiseToken;
  const owner = accounts[0];
  const other = accounts[1];
  const legacyDeposit = accounts[2];
  const newDeposit = accounts[3];

  const wallets = [owner, other, legacyDeposit, ...Array.from({ length: 200 }, () => getRandomAddress())];
  let balances = {};

  const beforeTests = async () => {
    balances = {};
    try {
      LegacyToken = await LegacyTokenContract.new();
      TokenNotOwner = await LegacyTokenContract.new({ from: other });
      ERC20Migrator = await ERC20MigratorContract.new(LegacyToken.address, legacyDeposit);
      RaiseToken = await RaiseTokenContract.new(LegacyToken.address, ERC20Migrator.address);
      // Move funds from Legacy Token
      await Bluebird.map(
        wallets,
        async account => {
          const amount = toWei(getRandomInt(100, 1000).toString(), "ether");
          await LegacyToken.transfer(account, amount);
          balances[account] = amount;
          return;
        },
        { concurrency: 50 }
      );
      const ownerBalance = (await LegacyToken.balanceOf(owner)).toString();
      balances[owner] = ownerBalance;
    } catch (error) {
      throw error;
    }
  };

  describe("Smart Contract Deployment", () => {
    beforeEach(async () => {
      LegacyToken = await LegacyTokenContract.new();
    });

    it("Should deploy if is an ERC20 and correct legacyDepositAddress", async () => {
      await truffleAssert.passes(
        ERC20MigratorContract.new(LegacyToken.address, legacyDeposit),
        "Deploy should pass"
      );
    });

    it("Should NOT deploy if legacyToken is not a detailed ERC20", async () => {
      await truffleAssert.fails(ERC20MigratorContract.new(owner, legacyDeposit));
    });

    it("Should NOT deploy if legacyToken is a zero address", async () => {
      await truffleAssert.fails(
        ERC20MigratorContract.new(ZERO_ADDRESS, legacyDeposit),
        "ERC20Migrator: legacy token is the zero address."
      );
    });

    it("Should NOT deploy if legacyDeposit is a zero address", async () => {
      await truffleAssert.fails(
        ERC20MigratorContract.new(LegacyToken.address, ZERO_ADDRESS),
        "ERC20Migrator: legacyDepositAddress_ is the zero address"
      );
    });
  });

  describe("beginMigration", () => {
    beforeEach(beforeTests);

    it("Should be able to start migration if ERC20Migrator have minter role", async () => {
      const receipt = await ERC20Migrator.beginMigration(RaiseToken.address, newDeposit);
      const result = await truffleAssert.createTransactionResult(ERC20Migrator, receipt.tx);
      truffleAssert.eventEmitted(
        result,
        "MigrationStarted",
        ev => ev.legacyToken === LegacyToken.address && ev.newToken === RaiseToken.address,
        "MigrationStarted should be emitted with correct parameters"
      );
    });

    it("Should NOT be able to start migration if ERC20Migrator does not have minter role", async () => {
      await truffleAssert.fails(
        ERC20Migrator.beginMigration(TokenNotOwner.address, newDeposit),
        "ERC20Migrator: not a minter for new token"
      );
    });

    it("Should NOT be able to start migration if newDeposit is zero Address", async () => {
      await truffleAssert.fails(
        ERC20Migrator.beginMigration(RaiseToken.address, ZERO_ADDRESS),
        "ERC20Migrator: depositContractAddress is the zero address"
      );
    });
  });

  describe("migrateBatch", () => {
    beforeEach(beforeTests);

    it("Should do the migration if started", async () => {
      let newDepositMigrated = false;
      const batches = chunk(wallets, 100);
      await truffleAssert.passes(ERC20Migrator.beginMigration(RaiseToken.address, newDeposit));
      await Bluebird.map(batches, async (batch, index) => {
        return truffleAssert.passes(ERC20Migrator.migrateBatch(batch), `Batch failed ${index + 1}`);
      });
      // Check all migrated balances should have equal new token as legacy token balance
      let newTokenCap = new BN("0");
      await Bluebird.map(
        wallets,
        async (account, index) => {
          const legacyBalance = balances[account];
          const raiseBalance = await RaiseToken.balanceOf(account);
          if (account === legacyDeposit) {
            newDepositMigrated = true;
            const legacyDepositOldBalance = await LegacyToken.balanceOf(legacyDeposit);
            const newDepositBalance = await RaiseToken.balanceOf(newDeposit);
            const newDepositOldBalance = await LegacyToken.balanceOf(newDeposit);
            newTokenCap = newTokenCap.add(newDepositBalance);
            // Check New Deposit Balance
            expect(newDepositOldBalance).to.eq.BN("0", 'NewDeposit balance in LegacyToken should be zero');
            expect(newDepositBalance).to.gte.BN("0");
            expect(newDepositBalance).to.gte.BN(new BN(legacyBalance));
            console.log(
              '\tNew DepositContract balance', '\n',
              '\t  - newToken: ', fromWei(newDepositBalance), '\n',
              '\t  - legacyToken: ', fromWei(newDepositOldBalance)
            )
            // Check Old Deposit Balance
            expect(raiseBalance).to.eq.BN("0", 'LegacyDeposit raise balance should be zero in new token');
            expect(legacyDepositOldBalance).to.eq.BN(legacyBalance, 'LegacyDeposit balance in LegacyToken should be the same');
            console.log(
              '\tLegacy DepositContract balance', '\n',
              '\t  - newToken: ', fromWei(raiseBalance), '\n',
              '\t  - legacyToken: ', fromWei(legacyDepositOldBalance)
            )
            return;
          }
          newTokenCap = newTokenCap.add(raiseBalance);
          expect(new BN(legacyBalance)).to.eq.BN(
            raiseBalance,
            `Account #${index} with address ${account} have no equal balance in new token.`
          );
          expect(raiseBalance).to.gte.BN("0");
        },
        { concurrency: 50 }
      );
      // Check New Deposit is migrated
      expect(newDepositMigrated).equals(true);
      // Check new token current cap should be equal than legacy supply
      const legacyCap = await LegacyToken.totalSupply();
      const newTokenCapCall = await RaiseToken.totalSupply();
      expect(newTokenCapCall).to.gte.BN("0");
      expect(newTokenCapCall).to.eq.BN(legacyCap);
      expect(newTokenCapCall).to.eq.BN(newTokenCap);
      expect(newTokenCap).to.gte.BN("0");
      expect(newTokenCap).to.eq.BN(legacyCap);
    });

    it("Should NOT do the migration if not started", async () => {
      const batches = chunk(wallets, 100);
      await Bluebird.map(batches, async (batch, index) => {
        return truffleAssert.fails(ERC20Migrator.migrateBatch(batch), 'ERC20Migrator: token is not set');
      });
    });
  });
});
