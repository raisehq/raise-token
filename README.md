# Raise Token smart contracts 

This repo contains the set of smart contracts related with the Raise (RAISE) token migration from Hero Origen (HERO).

# Get started

Clone this repository and install dependencies:
```
npm i
```

To be able to simulate the airdrop is needed to fork the Ethereum mainnet network, to be able to interact with legacy contract and migrate the holder balances to the new token. For this we use ganache `--fork` argument, pointing a local node or even Infura, once connected the ganache will start to query the blockchain when he needed to retrieve the state of an account or contract in a determinated block number.


```
# Start ganache in fork mode
npx ganache-cli --fork https://mainnet.infura.io/v3/your-infura-token

```

In another terminal, run and compile smart contracts as usual.

```
# Compile contracts
npm run compile

# Run migration in local forked ganache
npm run migrate
```

At the migration phase it will deploy the ERC20Migrator, a new token and after start with the airdrop process in batches. Once done you can connect to the local ganache and check the user balances have been exported to the new token.

## Keep in mind
This token migration does not burn or lock tokens in the legacy token. My recommendation is to at least `pause` the legacy token prior migrating to the new one.

## Supported use cases

The contracts support the following use cases:

* Migrate old token balances to a new token, delegating the mint process to `contracts/ERC20Migrator.sol` with a push strategy, airdrop-like. The smart contract checks the old balance in the legacy token and mints the exact amount in the new token. For this process, the legacy token must be paused.
* Capped supply token
