pragma solidity ^0.5.12;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Capped.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Pausable.sol";

contract RaiseToken is ERC20, ERC20Detailed, ERC20Capped, ERC20Pausable {
    constructor(IERC20 _oldToken, address _migrator)
        public
        ERC20Detailed("Raise", "RAISE", 18)
        ERC20Capped(_oldToken.totalSupply())
    {
        // Set migrator as the only minter, and renounce msg.sender as minter
        addMinter(_migrator);
        renounceMinter();
    }
}
