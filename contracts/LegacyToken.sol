pragma solidity ^0.5.12;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Capped.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Pausable.sol";

contract LegacyToken is ERC20, ERC20Detailed, ERC20Capped, ERC20Pausable {
    constructor() public ERC20Detailed("Legacy", "LCY", 18) ERC20Capped(10000000000000 ether) {
        // Mint all to msg sender
        mint(msg.sender, 10000000000000 ether);
        renounceMinter();
    }
}
