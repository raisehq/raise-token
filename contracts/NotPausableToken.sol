pragma solidity ^0.5.12;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Capped.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Pausable.sol";

contract NotPausableToken is ERC20, ERC20Detailed, ERC20Capped {
    constructor() public ERC20Detailed("Legacy Not Paused", "LNP", 18) ERC20Capped(100000 ether) {
        // Mint all to msg sender
        mint(msg.sender, 10000 ether);
        renounceMinter();
    }
}
